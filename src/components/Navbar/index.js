import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './style.css';
import { userLogout } from '../../store/actions/logoutAction';

class Navbar extends Component {
  logoutHandler = () => {
    this.props.dispatch(userLogout());
    console.log(this.props.logoutHandler);
  };

  render() {
    return (
      <div className='navbar'>
        <ul>
          <Link to='/feed'>
            <li>Feed</li>
          </Link>
          <Link to='/users/'>
            <li>Users</li>
          </Link>

          <Link to='/'>
            <li onClick={this.logoutHandler}>Logout</li>
          </Link>
        </ul>
      </div>
    );
  }
}

export default connect()(Navbar);
