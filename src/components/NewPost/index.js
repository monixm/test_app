import React, { Component } from 'react';
import { connect } from 'react-redux';
import { newPostAction } from '../../store/actions/newPostAction';

class NewPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: ''
    };
  }

  handleChange = event => {
    this.setState({
      content: event.currentTarget.value
    });
  };

  handleAddPost = e => {
    e.preventDefault();
    const action = newPostAction(this.state);
    this.props.dispatch(action);
    this.setState({ post: '' }); // clears the input field after submitting 
    console.log(this.state);
  };

  render() {
    return (
      <form>
        <input
          type='text'
          onChange={this.handleChange}
          value={this.state.content}
        ></input>
        <button onClick={this.handleAddPost}>Add Post</button>
      </form>
    );
  }
}

export default connect()(NewPost);
