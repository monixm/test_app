import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserListAction } from '../../store/actions/getUsersAction';
import Navbar from '../Navbar';
import './style.css';
import { followUserAction } from '../../store/actions/followAction';

class UserList extends Component {
  async componentDidMount() {
    this.props.dispatch(getUserListAction());
  }

  handleFollow = (user, e) => {
    // e.preventDefault();
    console.log(user.isFollowed);
    this.props.dispatch(followUserAction(user._id));
    // this.forceUpdate();
    console.log(user.isFollowed);
  };

  getUserDetails = id => {
    this.props.history.push(`/users/${id}`);
  };

  render() {
    return (
      <div>
        <Navbar />
        {this.props.users.map(user => {
          return (
            <div className='userList'>
              <img src={user.avatar} alt='' className='img'></img>
              <div
                className='desc'
                onClick={() => this.getUserDetails(user._id)}
              >
                <p>
                  <strong>{user.username}</strong>
                </p>
                <p>{user.email}</p>
                <p>{`Follows: ${user.follows.length}`}</p>
              </div>
              <div className='button'>
                <button onClick={e => this.handleFollow(user)}>
                  {user.isFollowed ? 'Unfollow' : 'follow'}
                </button>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.userListReducer.users,
    isFollowed: state.userListReducer.isFollowed
  };
};
export default connect(mapStateToProps)(UserList);
