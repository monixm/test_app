import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserDetailsAction } from '../../store/actions/getUserDetails';
import Navbar from '../Navbar';
import { followUserAction } from '../../store/actions/followAction';
import './style.css';

class UserDetails extends Component {
  async componentDidMount() {
    //get id
    const userId = this.props.match.params.userId;
    this.props.dispatch(getUserDetailsAction(userId));
  }

  handleFollow = user => {
    // console.log(user.isFollowed);
    this.props.dispatch(followUserAction(user._id));
    console.log(user.isFollowed);
  };

  render() {
    const { user } = this.props;
    return (
      <div className='container'>
        <Navbar />
        <div className='userInfo'>
          <img className='photo' alt='' src={user.avatar}></img>
          <h5>{user.username} </h5>
          <p>{user.email}</p>
          {/* // first map thru user then thru blitz to get content */}
          {/* {this.props.user._id.map(() => {
            return <Feed />;
          })} */}
          <button onClick={e => this.handleFollow(user)}>
            {user.isFollowed ? 'Unfollow' : 'Follow'}
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.userDetailsReducer.user,
    blitzs: state.userDetailsReducer.blitzs
  };
};

export default connect(mapStateToProps)(UserDetails);

// Render the user information of the user plus his/her blitzs.
// Current user should be able to Follow or Unfollow with a click.
