import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFeedAction } from '../../store/actions/getFeedAction';
import './style.css';
import NewPost from '../NewPost';
import Navbar from '../Navbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { likePostAction } from '../../store/actions/likePostAction';

class Feed extends Component {
  async componentDidMount() {
    this.props.dispatch(getFeedAction());
  }

  getUserDetails = id => {
    this.props.history.push(`/users/${id}`);
  };

  handleLike = post => {
    // console.log(post.isLiked);
    this.props.dispatch(likePostAction(post._id));
  };

  render() {
    return (
      <>
        <div>
          <Navbar />
        </div>
        <div className='feedComp'>
          <div className='newPost'>
            <NewPost />
          </div>
          <div className='feed'>
            {this.props.isLoading
              ? //if loading happening, the fetch is not finished,
                'Loading...'
              : this.props.feed.map(post => {
                  return (
                    <div className='post'>
                      <img
                        src={post._user.avatar}
                        className='img'
                        alt=''
                        onClick={() => this.getUserDetails(post._user._id)}
                      ></img>
                      <p className='username'>
                        <strong>{post._user.username}</strong>
                      </p>
                      <div className='post-content'>
                        <p>{post.content}</p>
                        <button
                          onClick={() => this.handleLike(post)}
                          className='heart'
                        >
                          {post.isLiked ? (
                            <FontAwesomeIcon icon={faHeart} className='red' />
                          ) : (
                            <FontAwesomeIcon icon={faHeart} className='grey' />
                          )}
                        </button>
                      </div>
                    </div>
                  );
                })}
          </div>
        </div>
      </>
    );
    // return this.props.isLoading
    //   ? //if loading happening, the fetch is not finished,
    //     'Loading...'
    //   : this.props.feed.map(post => {
    //       return <p>{post.content}</p>;
    //     });
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    feed: state.feedReducer.feed,
    isLoading: state.feedReducer.isLoading,
    isLiked: state.feedReducer.isLiked
  };
};

export default connect(mapStateToProps)(Feed);
