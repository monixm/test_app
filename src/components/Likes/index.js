import React, { Component } from 'react';
import { connect } from 'react-redux';
import { likePostAction } from '../../store/actions/likeAction';

class Likes extends Component {
  async componentDidMount() {
    this.props.dispatch(likePostAction());
  }

  render() {
    return (
      <>
        <div className='feed'>
          {this.props.feed.map(post => {
            return (
              <p className='username'>
                <strong>{post.likes}</strong>
              </p>
            );
          })}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    likes: state.feedReducer.likes
  };
};

export default connect(mapStateToProps)(Likes);
