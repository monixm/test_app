import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import store from './store';
import { userLogin } from './store/actions/loginAction';

// to make sure the user stays logged-in & that he's not thrown out upon a reload
const token = localStorage.getItem('token');
// if we have the token consider the user to be signed in
if (token) {
  // console.log('have token', token);
  store.dispatch(userLogin(token));
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
