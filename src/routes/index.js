import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Login from '../containers/Login';
import Feed from '../components/Feed';
import AuthComponent from '../HOC/authComponent';
import UserDetails from '../components/UserDetails';
import UserList from '../components/UserList';

class Routes extends Component {
  render() {
    return (
      <>
        <Route exact path='/' component={Login} />
        <Route exact path='/feed' component={AuthComponent(Feed)} />
        <Route exact path='/users' component={AuthComponent(UserList)} />
        <Route
          exact
          path='/users/:userId/follow'
          component={AuthComponent(UserList)}
        />
        <Route
          exact
          path='/users/:userId'
          component={AuthComponent(UserDetails)}
        />
        <Route
          exact
          path='/blitzs/:blitzId/like'
          component={AuthComponent()}
        />
      </>
    );
  }
}

export default Routes;
