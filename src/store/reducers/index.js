import { combineReducers } from 'redux';
import { userLoginReducer } from './userLoginReducer';
import { feedReducer } from '../reducers/feedReducer';
import { userLogoutReducer } from './userLogoutReducer';
import { userListReducer } from './userListReducer';
import { userDetailsReducer } from './userDetailsReducer';

export const reducers = combineReducers({
  userLoginReducer,
  feedReducer,
  userDetailsReducer,
  userListReducer,
  userLogoutReducer
});
