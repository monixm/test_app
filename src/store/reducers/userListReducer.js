import { GET_USERS, FOLLOW_USER } from '../actions/types';

const initialState = {
  users: [],
  userId: [],
  isFollowed: false
};

export const userListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS: {
      return { ...state, users: action.payload };
    }
    case FOLLOW_USER: {
      const newUser = { ...state };
      const userId = newUser.filter(user => user.id === action.payload);
      const user = { ...state[newUser] };
      user.followed = !user.followed;
      const newState = [...state];
      newState[userId] = user;
      return newUser;
    }
    default:
      return state;
  }
};
