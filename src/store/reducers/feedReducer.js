import { GET_FEED, NEW_POST, LIKE_POST, GET_LIKES } from '../actions/types';

const inititalState = {
  feed: [],
  isLoading: true,
  isLiked: false,
  likes: []
};

export const feedReducer = (state = inititalState, action) => {
  switch (action.type) {
    case GET_FEED: {
      return { ...state, feed: action.payload, isLoading: false };
      // const newState = [...state];
      // newState.feed = action.payload;
      // return newState;
    }
    case NEW_POST: {
      const post = action.payload;
      const newState = { ...state };
      const feed = [...state.feed];
      const newFeed = feed.concat([post]);
      newState.feed = newFeed;
      return newState;
    }
    case LIKE_POST: {
      const newPost = { ...state };
      const postId = newPost.filter(post => post.id === action.payload);
      const post = { ...state[newPost] };
      post.liked = !post.liked;
      const newState = [...state];
      newState[postId] = post;
      return newState;
    }
    case GET_LIKES: {
      return { ...state, likes: action.payload };
    }
    default:
      return state;
  }
};
