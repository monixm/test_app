import { USER_LOGIN_SUCCESS } from '../actions/types';

const initialState = {
  token: null,
  authenticated: null
};

export const userLoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN_SUCCESS: {
      return { ...state, token: action.payload, authenticated: true };
    }
    default:
      return state;
  }
};
