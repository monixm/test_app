import { baseUrl } from '../constants';
import { USER_LOGIN_SUCCESS } from '../actions/types';

export const userLogin = token => ({
  type: USER_LOGIN_SUCCESS,
  payload: token
});

export const userLoginAction = (email, password) => async (
  dispatch,
  getState
) => {
  const headers = new Headers({
    'Content-Type': 'application/json'
  });

  const body = { email, password };

  const config = {
    body: JSON.stringify(body),
    headers,
    method: 'POST'
  };

  const response = await fetch(`${baseUrl}api/login`, config);
  const user = await response.json();
  const { token } = user;
  console.log(user);
  if (token) {
    localStorage.setItem('token', token);
  }
  dispatch(userLogin(token));
  return token;
};

// try {
//   const response = await fetch(
//     `https://propulsion-blitz.herokuapp.com/api/login`,
//     config
//   );
//   const userLoginAction = await response.json();
//   // console.log(userLoginAction.token);
//   console.log(userLoginAction);
//   localStorage.setItem('token', userLoginAction.token);
//   if (response.status !== 200) {
//     throw new Error('errrorrrr');
//   }
// } catch (e) {
//   console.log('Error');
// }
