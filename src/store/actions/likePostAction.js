import { baseUrl } from '../constants';
import { getFeedAction } from './getFeedAction';

export const likePostAction = blitzId => async (dispatch, getState) => {
  let { token } = getState().userLoginReducer;
  if (!token) {
    token = localStorage.getItem('token');
  }

  const headers = new Headers({
    'Content-type': 'application/json',
    Authorization: `Bearer ${token}`
  });

  const config = {
    headers,
    method: 'POST'
  };

  const response = await fetch(`${baseUrl}api/blitzs/${blitzId}/like`, config);
  const post = await response.json();
  console.log(post);
  dispatch(getFeedAction());
};
