import { baseUrl } from '../constants';
import { GET_SINGLE_POST } from './types';

const getUserDetails = post => {
  return {
    type: GET_SINGLE_POST,
    payload: post
  };
};

export const getUserDetailsAction = postId => async (dispatch, getState) => {
  let { token } = getState().userLoginReducer;
  if (!token) {
    token = localStorage.getItem('token');
  }
  const headers = new Headers({
    'Content-type': 'application/json',
    Authorization: `Bearer ${token}`
  });

  const config = {
    headers,
    method: 'GET'
  };

  const response = await fetch(`${baseUrl}api/blitzs/${postId}`, config);
  const post = await response.json();
  dispatch(getUserDetails(post));
};
