import { baseUrl } from '../constants';
import { GET_FEED } from './types';

const getFeed = feed => {
  return {
    type: GET_FEED,
    payload: feed
  };
};

export const getFeedAction = () => async (dispatch, getState) => {
  console.log('getFeedAction');
  let { token } = getState().userLoginReducer;
  if (!token) {
    token = localStorage.getItem('token');
  }

  const headers = new Headers({
    'Content-type': 'application/json',
    Authorization: `Bearer ${token}`
  });

  const config = {
    headers,
    method: 'GET'
  };

  //tutaj musi zostac api bo pobieram dane z backendu
  const response = await fetch(`${baseUrl}api/feed`, config);
  const feed = await response.json();
  console.log(feed);
  dispatch(getFeed(feed));
  //dispatch action to redux store to store posts
};
