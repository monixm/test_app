import { LOGOUT } from '../actions/types';

export const userLogout = () => {
  localStorage.clear();
  return {
    type: LOGOUT
  };
};

