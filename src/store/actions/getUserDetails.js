import { baseUrl } from '../constants';
import { GET_USER_DETAILS } from './types';

const getUserDetails = user => {
  return {
    type: GET_USER_DETAILS,
    payload: user,
    
  };
};

export const getUserDetailsAction = userId => async (dispatch, getState) => {
  let { token } = getState().userLoginReducer;
  if (!token) {
    token = localStorage.getItem('token');
  }
  const headers = new Headers({
    'Content-type': 'application/json',
    Authorization: `Bearer ${token}`
  });

  const config = {
    headers,
    method: 'GET'
  };

  const response = await fetch(`${baseUrl}api/users/${userId}`, config);
  const user = await response.json();
  dispatch(getUserDetails(user));
};
