import { baseUrl } from '../constants';
import { GET_USERS } from '../actions/types';

export const getUsersAction = users => {
  return {
    type: GET_USERS,
    payload: users
  };
};

export const getUserListAction = () => async (dispatch, getState) => {
  // console.log('getUserList');
  let { token } = getState().userLoginReducer;
  if (!token) {
    token = localStorage.getItem('token');
  }

  const headers = new Headers({
    'Content-type': 'application/json',
    Authorization: `Bearer ${token}`
  });

  const config = {
    headers,
    method: 'GET'
  };

  //tutaj musi zostac api bo pobieram dane z backendu
  const response = await fetch(`${baseUrl}api/users`, config);
  const users = await response.json();
  // console.log(users);
  dispatch(getUsersAction(users));
};
