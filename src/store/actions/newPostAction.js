import { baseUrl } from '../constants';
import { NEW_POST } from './types';

export const newPost = post => {
  return {
    type: NEW_POST,
    payload: post
  };
};

export const newPostAction = content => async (dispatch, getState) => {
  // console.log('newPostAction');
  let { token } = getState().userLoginReducer;
  if (!token) {
    token = localStorage.getItem('token');
  }

  const headers = new Headers({
    'Content-type': 'application/json',
    Authorization: `Bearer ${token}`
  });

  const config = {
    headers,
    method: 'POST',
    body: JSON.stringify(content)
  };

  //tutaj musi zostac api bo pobieram dane z backendu
  const response = await fetch(`${baseUrl}api/blitzs`, config);
  const post = await response.json();
  // console.log('the post', post);
  dispatch(newPost(post));
};
