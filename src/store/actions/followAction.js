import { baseUrl } from '../constants';
import { getUserListAction } from './getUsersAction';

export const followUserAction = userId => async (dispatch, getState) => {
  console.log('followUser');
  let { token } = getState().userLoginReducer;
  if (!token) {
    token = localStorage.getItem('token');
  }

  const headers = new Headers({
    'Content-type': 'application/json',
    Authorization: `Bearer ${token}`
  });

  const config = {
    headers,
    method: 'POST'
  };

  //tutaj musi zostac api bo pobieram dane z backendu
  const response = await fetch(`${baseUrl}api/users/${userId}/follow`, config);
  const user = await response.json();
  console.log(user);
  dispatch(getUserListAction());
};
